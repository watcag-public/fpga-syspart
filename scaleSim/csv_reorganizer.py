import numpy as np
import pandas as pd
import os.path
import sys

if __name__ == "__main__":
    path = sys.argv[1]
    runname = sys.argv[2]
    out_path = sys.argv[3]
    maxsize = int(sys.argv[4])

    frames = []

    for i in range(maxsize):
        cycle_csv_path  = path + runname + "_ws_9x" + str(i+1) + "/" + runname + "_cycles.csv"
        detail_csv_path = path + runname + "_ws_9x" + str(i+1) + "/" + runname + "_detail.csv"

        if not os.path.exists(cycle_csv_path):
            cycle_csv_path  = path + runname + "_ws_9x" + str(i+1) + "/9x" + str(i+1) + "_" + runname + "_cycles.csv"
            detail_csv_path = path + runname + "_ws_9x" + str(i+1) + "/9x" + str(i+1) + "_" + runname + "_detail.csv"

        basic_cycle = pd.read_csv(cycle_csv_path)
        detail = pd.read_csv(detail_csv_path)

        # remove unname column
        basic_cycle = basic_cycle.loc[:, ~basic_cycle.columns.str.contains('^Unnamed')]
        # basic_cycle = basic_cycle.loc[:, ~basic_cycle.columns.str.contains('% Utilization')]

        detail = detail.loc[:, ~detail.columns.str.contains('^Unnamed')]

        detail.columns = [col.strip('\t') for col in detail.columns]
        basic_cycle.columns = [col.strip('\t') for col in basic_cycle.columns]

        detail['DRAM_cycle'] = detail['DRAM_OFMAP_stop'] - detail['DRAM_IFMAP_start']
        detail['SRAM_cycle'] = detail['SRAM_write_stop'] - detail['SRAM_read_start']

        df = pd.merge(basic_cycle, detail, on='Layer')
        df['ArraySize'] = [(i+1)] * len(detail['Layer'])

        frames.append(df)
    
    result = pd.concat(frames)

    cols = result.columns.tolist()
    cols = [cols[-1]] + [cols[0]] + cols[-3:-1] + cols[1:-3]
    result = result[cols]

    result = result.sort_values(by=['Layer', 'ArraySize'], ascending=True)
    result.to_csv(out_path, index = False)

    # python3 csv_reorganizer.py ../outputs/ alexnet ./testing.csv 10
