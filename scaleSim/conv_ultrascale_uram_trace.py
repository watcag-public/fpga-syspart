import os.path
import math
from tqdm import tqdm

import conv_ultrascale_trace_gen as trace_utility

def uram_trace_read(
        internal_ram_sz = 288000/8, #Byte
        word_sz_bytes = 1,
        ifmap_min_addr=0, ifmap_max_addr=1000000,
        filter_min_addr=1000000, filter_max_addr=2000000,
        input_bw = 8, 
        ifmap_out_bw = 4, filter_out_bw = 4,
        bram_trace_file = "ultra_bram_read.csv",
        uram_trace_file = "ultra_uram_read.csv"
    ):

    ifmap_content = set()
    filter_content = set()

    patch_first_clk = -1
    cycle_accumulate = -1
    is_first_fetch = True

    filter_filled, ifmap_filled, filter_filling, ifmap_filling = [False]*4

    bram_requests = open(bram_trace_file, 'r')
    clear = open(uram_trace_file, 'w')
    clear.close

    for entry in bram_requests:
        clk, elems = trace_utility.turn_string_to_elements(entry)
        
        if len(elems) > 0:
            #print(elems, filter_filling, filter_filled, ifmap_filling, ifmap_filled)
            #fill filter
            if filter_min_addr <= elems[0] < filter_max_addr and not filter_filled:
                for e in range(0, len(elems)):
                    filter_content.add(int(elems[e]))
                
                # if this the first filter in this patch
                if not filter_filling:
                    patch_first_clk = clk
                
                filter_filling = True
            else:
                filter_filled = True and filter_filling
            
            #fill ifmap
            if ifmap_min_addr <= elems[0] < ifmap_max_addr and not ifmap_filled:
                for e in range(0, len(elems)):
                    ifmap_content.add(int(elems[e]))
                ifmap_filling = True
            else:
                ifmap_filled = True and filter_filled and ifmap_filling

        #print ifmap and filter
        if filter_filled and ifmap_filled:
            cycle_accumulate = trace_utility.gen_uram_trace(
                ifmap_addr = sorted(ifmap_content,reverse = True),
                filter_addr = sorted(filter_content,reverse = True),
                ifmap_ram_size = internal_ram_sz, word_sz = word_sz_bytes,
                input_bw = input_bw,
                ifmap_out_bw = ifmap_out_bw, filter_out_bw = filter_out_bw,
                last_cycle = cycle_accumulate, patch_first_drain_clk = patch_first_clk,
                first_ever_fetch = is_first_fetch,
                output_file = uram_trace_file)
            filter_filled, ifmap_filled, filter_filling, ifmap_filling = [False]*4
            is_first_fetch = False

            filter_content.clear
            ifmap_content.clear

    #print(elems, filter_filling, filter_filled, ifmap_filling, ifmap_filled)
    if filter_filled and not ifmap_filled and len(ifmap_content) != 0:
        cycle_accumulate = trace_utility.gen_uram_trace(
            ifmap_addr = sorted(ifmap_content,reverse = True),
            filter_addr = sorted(filter_content,reverse = True),
            ifmap_ram_size = internal_ram_sz, word_sz = word_sz_bytes,
            input_bw = input_bw,
            ifmap_out_bw = ifmap_out_bw, filter_out_bw = filter_out_bw,
            last_cycle = cycle_accumulate,
            first_ever_fetch = is_first_fetch,
            output_file = uram_trace_file)
        filter_content.clear
        ifmap_content.clear

    bram_requests.close()

# row based partition
if __name__ == "__main__":
    # uram_trace_read(
    #     internal_ram_sz=math.floor((288000)/8 * 4),
    #     input_bw = 8, 
    #     ifmap_out_bw = 4, filter_out_bw = 4,
    #     ifmap_min_addr=0, ifmap_max_addr=10000000,
    #     filter_min_addr=10000000, filter_max_addr=20000000,
    #     word_sz_bytes = 1,
    #     bram_trace_file="test_out/ultra_bram_read_sc.csv",
    #     uram_trace_file="test_out/ultra_uram_read_sc.csv")

    # uram_trace_read(
    #     internal_ram_sz=math.floor((288000)/8 * 4),
    #     input_bw = 8, 
    #     ifmap_out_bw = 4, filter_out_bw = 4,
    #     ifmap_min_addr=0, ifmap_max_addr=10000000,
    #     filter_min_addr=10000000, filter_max_addr=20000000,
    #     word_sz_bytes = 1,
    #     bram_trace_file="test_out/ultra_bram_read_g1.csv",
    #     uram_trace_file="test_out/ultra_uram_read_g1.csv")

    # uram_trace_read(
    #     internal_ram_sz=math.floor((288000)/8 * 4),
    #     input_bw = 8, 
    #     ifmap_out_bw = 4, filter_out_bw = 4,
    #     ifmap_min_addr=0, ifmap_max_addr=10000000,
    #     filter_min_addr=10000000, filter_max_addr=20000000,
    #     word_sz_bytes = 1,
    #     bram_trace_file="test_out/ultra_bram_read_scmf.csv",
    #     uram_trace_file="test_out/ultra_uram_read_scmf.csv")

    uram_trace_read(
        internal_ram_sz=math.floor((288000)/8 * 4),
        input_bw = 8, 
        ifmap_out_bw = 4, filter_out_bw = 4,
        ifmap_min_addr=0, ifmap_max_addr=10000000,
        filter_min_addr=10000000, filter_max_addr=20000000,
        word_sz_bytes = 1,
        bram_trace_file="test_out/100x100_3x3_60_bram_read.csv",
        uram_trace_file="test_out/100x100_3x3_60_uram_read.csv")