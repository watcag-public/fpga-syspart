#!/bin/bash
# ./generate_final_csv.sh yolo_tiny optimization_algo/yolo_tiny_mem_bound.csv 960

if [ "$1" != "" ]; then
    python3 csv_reorganizer.py outputs/ $1 $2 $3
else
    echo "Positional parameter 1 is empty"
fi