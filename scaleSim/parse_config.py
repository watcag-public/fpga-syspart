import configparser as cp
from absl import flags

def array_step_parse(array_string):
    ar = []
    ar_st = array_string.split(',')
    ar.append(int(ar_st[0].strip()))

    if len(ar_st) > 1:
        step = int(ar_st[2].strip())
        value = ar[0]
        max_value = int(ar_st[1].strip())
        while value < max_value:
            value += step
            ar.append(value)
    return ar

def parse_config(array_config_file, dnn_topology_file):
    general  = 'general'
    arch_sec = 'architecture_presets'
    net_sec  = 'network_presets'
    sub_mem_arch = 'memory_structure'

    setting_items = ["InternalRamSize", "IfmapInBW", "FilterInBW", "IfmapOutBW", "FilterOutBW"]
    array_setting = {}
    sub_ram_setting = {}

    config_filename = array_config_file

    config = cp.ConfigParser()
    config.read(config_filename)

    ## Runname
    run_name = config.get(general, 'run_name')

    ## Systolic Array settings
    ar_h = array_step_parse(config.get(arch_sec, 'ArrayHeight'))
    ar_w = array_step_parse(config.get(arch_sec, 'ArrayWidth'))

    ifmap_offset    = int(config.get(arch_sec, 'IfmapOffset').strip())
    filter_offset   = int(config.get(arch_sec, 'FilterOffset').strip())
    ofmap_offset    = int(config.get(arch_sec, 'OfmapOffset').strip())

    array_setting = {
        'ArrayHeight'   : ar_h,
        'ArrayWidth'    : ar_w,
        'IfmapOffset'   : ifmap_offset,
        'FilterOffset'  : filter_offset,
        'OfmapOffset'   : ofmap_offset
    }

    ## Sub-ram settings
    ram_arch = config.get(sub_mem_arch, 'InnerLayers').strip().replace(" ", "").split(',')

    for ram in ram_arch:
        settings = {}
        for setting in setting_items:
            settings[setting] = int(config.get(ram, setting))
        sub_ram_setting[ram] = settings
        
    ## Read network_presets
    topology_file = open(dnn_topology_file, 'r')
    first = True
    topology = {}

    for row in topology_file:
        if first:
            first = False
            continue
            
        elems = row.strip().split(',')

        topology[elems[0]] = {
            'ifmap_h'       : int(elems[1]),
            'ifmap_w'       : int(elems[2]),
            'filt_h'        : int(elems[3]),
            'filt_w'        : int(elems[4]),
            'num_channels'  : int(elems[5]),
            'num_filters'   : int(elems[6]),
            'strides'       : int(elems[7])
        }
        
    topology_file.close()
    
    return run_name, array_setting, sub_ram_setting, topology

if __name__ == "__main__":
    print(parse_config("./configs/UltraScale.cfg", "./topologies/alexnet.csv"))