import time
import csv
import sys
import random
import operator
import path_constant as pc

from multiprocessing import Pool
from os import cpu_count
from functools import partial

def swap_random(l):
    idx = range(len(l))
    i1, i2 = random.sample(idx, 2)
    new_l = l.copy()
    new_l[i1], new_l[i2] = l[i2], l[i1]
    return new_l

class ga_approach(object):
    def __init__(self,
        path_to_datasrc = "googlenet_mem_bound.csv",
        path_to_topology = "googlenet.csv",
        target_col = "DRAM_cycle",

        # problem definition
        number_of_partition = 4, max_generation = 100,
        population_size = 100,
        elite_population = 10,
        crossover_prob = 0.7,
        mutation_prob = 0.6,

        max_res_unit = 960, initial_res = 0,
        res_step = 1,
        penalty_offest = 100000000000
        ):
        
        self.target_col = target_col
        self.start = time.time()

        self.k = number_of_partition
        self.max_gen = max_generation
        self.max_res_unit = max_res_unit
        self.res_step = res_step
        self.pop_size = population_size
        self.elite_pop_size = elite_population
        self.penalty_offest = penalty_offest
        self.ending_iter = 0
        self.data_src = {}
        self.crossover_prob = crossover_prob
        self.mutation_prob_og = mutation_prob
        self.mutation_prob_ad = mutation_prob

        self.topology_file = path_to_topology
        self.layers = self.parse_topology_file()
        self.parse_data_set_file(path_to_datasrc)

        self.best_gene = list(range(len(self.layers) - 1))

        self.total_valid_solution = 0
        self.trial = 1

    def seeding_generation(self):
        og = list(range(len(self.layers) - 1))
        pop = []

        for _ in range(self.pop_size):
            pop.append(swap_random(og))

        return pop

    def parse_topology_file(self):
        layers = []
        with open(pc.TOPOLOGIES_PATH+self.topology_file, 'r') as f:
            next(f)
            for line in f:
                elems = line.strip().split(',')
                layers.append(elems[0])

        for layer in layers:
            self.data_src[layer] = {}
        return layers

    def parse_data_set_file(self, path_to_data_csv):
        first = True
        target_idx = 2
        with open(pc.DATA_SOURCE_PATH+path_to_data_csv, 'r') as f:
            for line in f:
                elems = line.strip().split(',')
                # #print(elems)
                if first:
                    for idx, col in enumerate(elems):
                        if self.target_col in col:
                            target_idx = idx
                            break
                    first = False
                else:
                    self.data_src[elems[1]][int(elems[0])] = int(float(elems[target_idx]))

    def decode_gene(self, gene):
        useful_gene = gene[0:k-1]
        useful_gene.sort()
        solution_layer_domain = []
        part = []
        idx = 0

        for idx, l in enumerate(self.layers):
            if not useful_gene:
                part = self.layers[idx:]
                solution_layer_domain.append(part)
                break
            
            part.append(l)
            if idx == useful_gene[0]:
                solution_layer_domain.append(part)
                part = []
                useful_gene.pop(0)

        return solution_layer_domain

    def find_max_latency(self, layer_partition, res_partitions):
        latencies = [0] * len(layer_partition)
        max_latency_idx = 0

        for idx, part in enumerate(layer_partition):
            res = res_partitions[idx]
            for layer in part:
                latencies[idx] += self.data_src[layer][res]
            if latencies[idx] > latencies[max_latency_idx]:
                max_latency_idx = idx

        return latencies, max_latency_idx

    def evaluate_hybird(self, gene):
        layer = self.decode_gene(gene)
        res = [self.res_step] * self.k
        latencies = []

        for _ in range(0, int(self.max_res_unit/self.res_step - self.k*self.res_step)):
            latencies, max_idx = self.find_max_latency(layer, res)
            res[max_idx] += self.res_step

        return latencies[max_idx], latencies, res, layer, gene

    def evaluate(self, genes):
        # evaluate all possible population
        # return [(score1, gene1), (score1, gene1), ...]
        rankings = []
        pool = Pool(processes = (cpu_count() - 4))
        for result in pool.imap_unordered(self.evaluate_hybird, genes):
            score = result[0]
            gene = result[4]
            rankings.append((score, gene))
        pool.close()
        pool.join()

        rankings.sort(key = operator.itemgetter(0))
        return rankings

    def crossover(self, parents):
        mom, dad = parents
        if random.uniform(0, 1) < self.crossover_prob:
            st = random.randint(0, self.k - 1)
            end = random.randint(0, len(mom) - 1)

            while end == st:
                end = random.randint(0, len(mom) - 1)

            if end < st:
                st, end = end, st
            # #print(st, end)

            child_m, child_d = [-1]*len(mom), [-1]*len(mom)
            child_m[st:end] = mom[st:end]
            child_d[st:end] = dad[st:end]
            # #print(child_m, child_d)

            c_idx = end
            cursor = end
            while True:
                if dad[cursor] not in child_m:
                    child_m[c_idx]  = dad[cursor]
                    c_idx += 1
                
                cursor += 1
                if cursor == len(mom):
                    cursor = 0

                if c_idx == len(mom):
                    c_idx = 0

                if -1 not in child_m:
                    break
            
            c_idx = end
            cursor = end
            while True:
                if mom[cursor] not in child_d:
                    child_d[c_idx]  = mom[cursor]
                    c_idx += 1
                
                cursor += 1
                if cursor == len(mom):
                    cursor = 0

                if c_idx == len(mom):
                    c_idx = 0
                    
                if -1 not in child_d:
                    break

            return child_m, child_d
        else:
            return mom, dad

    def mutation(self, gene):
        if random.uniform(0, 1) < self.mutation_prob_ad:
            gene = swap_random(gene)
        return gene

    def run(self):
        current_pop = self.seeding_generation()
        last_best_latency = self.penalty_offest

        self.gen = 0
        non_improving_cnt = 0
        while self.gen < self.max_gen and non_improving_cnt < 10:
            start = time.time()

            # Evaluate current population
            ranking = self.evaluate(current_pop)
            gen_best_score = ranking[0][0]
            #print("Gen: ", self.gen, "Score: ", gen_best_score)

            if gen_best_score < last_best_latency:
                print("!! Improved by", (last_best_latency - gen_best_score) /last_best_latency * 100)
                last_best_latency = gen_best_score
                non_improving_cnt = 0
                self.mutation_prob_ad = self.mutation_prob_og
            else:
                non_improving_cnt += 1
                self.mutation_prob_ad *= 0.1
            
            done_eva = time.time()

            # Pick the best 10
            elite_pop = []
            self.best_gene = ranking[0][1]
            for i in range(self.elite_pop_size):
                current_pop.append(ranking[i][1])
                elite_pop.append(ranking[i][1])

            # Parental Selection
            parent_list = []
            for _ in range(int((self.pop_size - self.elite_pop_size)/2)):
                i1, i2 = random.sample(range(len(elite_pop)), 2)
                parent_list.append((elite_pop[i1], elite_pop[i2]))
            
            # Generate Offspring 
            pool = Pool(processes = (cpu_count() - 4))
            for result in pool.imap_unordered(self.crossover, parent_list):
                child_a, child_b = result[0], result[1]
                current_pop.append(self.mutation(child_a))
                current_pop.append(self.mutation(child_b))
            pool.close()
            pool.join()

            self.end = time.time()
            done_generate_offpsring = time.time()

            # print("Total:", done_generate_offpsring - start)
            # print(" Evaluation:", done_eva - start, "-", \
            #     (done_eva - start)/(done_generate_offpsring - start) * 100, "%")
            # print(" OffSpring:", done_generate_offpsring - done_eva, "-", \
            #     (done_generate_offpsring - done_eva)/(done_generate_offpsring - start) * 100, "%")
            self.report()
            self.gen += 1

    def report(self):
        max_latency, latencies, res, layer, _ = \
            self.evaluate_hybird(self.best_gene)

        # generate data for mapping the full array
        full_latency, full_max_idx = \
            self.find_max_latency([self.layers], [self.max_res_unit]*len(self.layers))

        # PLEASE UNCOMMENT OUT THIS PART IF YOU NOT USING THE BASH SCRIPT WE HAVE PROVIDED 
        # print("================================= RESULT =================================")
        # print(layer)
        # print("Res mapping:")
        # print(res)
        # print("Latency for each partition: ")
        # print(latencies)
        # print("Final Latency:", max_latency*self.k, "|| Throught put:", 1/max_latency)
        # print("==========================================================================")
        # print("Map to full array (", self.max_res_unit, ")")
        # print("Final Latency:", full_latency[full_max_idx], "|| Throught put:", 1/full_latency[full_max_idx])
        # print("==========================================================================")
        # print("Throughtput Ratio:", (1/max_latency)/(1/full_latency[full_max_idx]))
        # print("Latency increase:", (max_latency*self.k)/full_latency[full_max_idx])
        # PLEASE UNCOMMENT OUT THIS PART IF YOU NOT USING THE BASH SCRIPT WE HAVE PROVIDED 

        with open(pc.RESULT_CSV_PATH+'ga.csv', 'a') as csvFile:
            writer = csv.writer(csvFile, delimiter=',', lineterminator="\n")
            writer.writerow([self.target_col, self.gen,self.k,self.topology_file, 1,
                (1/max_latency), max_latency*self.k,
                1/full_latency[full_max_idx], full_latency[full_max_idx],
                (1/max_latency)/(1/full_latency[full_max_idx]),
                (max_latency*self.k)/full_latency[full_max_idx], 
                layer, res, 
                self.end-self.start, 0, 0, 100, 
                1,self.pop_size,self.max_res_unit,"ga"])
        csvFile.close

if __name__ == "__main__":
    # python3 ga_approach.py googlenet 20 10 100 960 DRAM_cycle
    topology = sys.argv[1]
    k = int(sys.argv[2])
    elite_population_size = int(sys.argv[3])
    population_size = int(sys.argv[4])
    max_res_unit = int(sys.argv[5])
    target_col = sys.argv[6]

    ga = ga_approach(
        path_to_datasrc = str(topology)+"_mem_bound.csv",
        path_to_topology = str(topology)+".csv",
        target_col = str(target_col),

        number_of_partition = k, max_generation = 10000,
        population_size = population_size,
        elite_population = 10,
        crossover_prob = 0.75,
        mutation_prob = 0.7,

        max_res_unit = 960, initial_res = 0,
        res_step = 1,
        penalty_offest = 100000000000
    )
    # #print(ga.seeding_generation()[0:k-1])
    # #print(ga.decode_gene([1,4]))
    ga.run()
    # ga.report()
