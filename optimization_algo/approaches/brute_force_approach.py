import sys
import time
import csv
import path_constant as pc

from tqdm import tqdm
from itertools import combinations


class brute_force(object):
    def __init__(self, 
        # data path
        path_to_datasrc = "googlenet_mem_bound.csv",
        path_to_topology = "googlenet.csv",
        target_col = "DRAM_cycle",
        
        # problem definition
        number_of_partition = 4,

        # constraint
        max_res_available = 960, initial_res = 0, 
        res_step = 1,
        ):

        self.topology_file = path_to_topology

        self.k = number_of_partition
        self.best_candidate = [0] * number_of_partition

        self.max_res_unit = max_res_available
        self.res_step = res_step

        self.target_col = target_col
        self.data_src = {}
        self.layers = self.parse_topology_file()
        self.parse_data_set_file(path_to_datasrc)
        self.ending_iter = 0

        self.start = time.time()

    def parse_topology_file(self):
        layers = []
        with open(pc.TOPOLOGIES_PATH+self.topology_file, 'r') as f:
            next(f)
            for line in f:
                elems = line.strip().split(',')
                layers.append(elems[0])

        for layer in layers:
            self.data_src[layer] = {}
        return layers
    
    def parse_data_set_file(self, path_to_data_csv):
        first = True
        target_idx = 2
        with open(pc.DATA_SOURCE_PATH+path_to_data_csv, 'r') as f:
            for line in f:
                elems = line.strip().split(',')
                # #print(elems)
                if first:
                    for idx, col in enumerate(elems):
                        if self.target_col in col:
                            target_idx = idx
                            break
                    first = False
                else:
                    self.data_src[elems[1]][int(elems[0])] = int(float(elems[target_idx]))

    def decode_gene(self, gene):
        gene = list(gene)
        solution_layer_domain = []
        part = []
        idx = 0

        for idx, l in enumerate(self.layers):
            if not gene:
                part = self.layers[idx:]
                solution_layer_domain.append(part)
                break
            
            part.append(l)
            if idx == gene[0]:
                solution_layer_domain.append(part)
                part = []
                gene.pop(0)

        return solution_layer_domain

    def generate_gene(self):
        return list(combinations(list(range(len(self.layers) - 1)), self.k-1))
        # for g in genes:
        #     print(g, self.decode_gene(g))
        # print("sizes:", len(genes))

    def find_max_latency(self, layer_partition, res_partitions):
        latencies = [0] * len(layer_partition)
        max_latency_idx = 0

        for idx, part in enumerate(layer_partition):
            res = res_partitions[idx]
            for layer in part:
                latencies[idx] += self.data_src[layer][res]
            if latencies[idx] > latencies[max_latency_idx]:
                max_latency_idx = idx

        return latencies, max_latency_idx

    def evaluate_hybird(self, gene):
        layer = self.decode_gene(gene)
        res = [self.res_step] * self.k
        latencies = []

        for _ in range(0, int(self.max_res_unit/self.res_step - self.k*self.res_step)):
            latencies, max_idx = self.find_max_latency(layer, res)
            res[max_idx] += self.res_step

        return latencies[max_idx], latencies, res, layer, gene     

    def run(self):
        min_latency = 100000000
        for g in tqdm(self.generate_gene()):
            lat, _, _, _, _ = self.evaluate_hybird(g)
            if lat < min_latency:
                self.best_candidate = g
                min_latency = lat

        self.end = time.time()

    def report(self):
        max_latency = 0
        layer = []
        res = []
        latencies = []
        full_latency, full_max_idx = self.find_max_latency([self.layers], [self.max_res_unit]*len(self.layers))
        max_latency = 100000000

        max_latency, latencies, res, layer, _ = self.evaluate_hybird(self.best_candidate)
        
        print("================================= RESULT =================================")
        print("Layer assignment:")
        print(layer)
        print("Res mapping:")
        print(res)
        print("Latency for each partition: ")
        print(latencies)
        print("Final Latency:", max_latency*self.k, "|| Throught put:", 1/max_latency)
        print("==========================================================================")
        print("Map to full array (", self.max_res_unit, ")")
        print("Final Latency:", full_latency[full_max_idx], "|| Throught put:", 1/full_latency[full_max_idx])
        print("==========================================================================")
        print("Throughtput Ratio:", (1/max_latency)/(1/full_latency[full_max_idx]))
        print("Latency increase:", (max_latency*self.k)/full_latency[full_max_idx])

        with open(pc.RESULT_CSV_PATH+'bruteforce.csv', 'a') as csvFile:
            writer = csv.writer(csvFile, delimiter=',', lineterminator="\n")
            writer.writerow([self.target_col,self.k, self.topology_file, 1,(1/max_latency), max_latency*self.k, 1/full_latency[full_max_idx], full_latency[full_max_idx], (1/max_latency)/(1/full_latency[full_max_idx]), (max_latency*self.k)/full_latency[full_max_idx], layer, res, self.end-self.start, self.max_res_unit])
        csvFile.close

if __name__ == "__main__":

    print("Brute Force")
    # python3 brute_force_approach.py googlenet 20 960 DRAM_cycle
    topology = sys.argv[1]
    k = int(sys.argv[2])
    max_res_unit = int(sys.argv[3])
    target_col = sys.argv[4]

    bf = brute_force(
        path_to_datasrc = str(topology)+"_mem_bound.csv",
        path_to_topology = str(topology)+".csv",
        target_col = str(target_col),

        number_of_partition = k,

        max_res_available = max_res_unit, initial_res = 0, 
        res_step = 1
    )

    # bf.generate_gene()
    bf.run()
    bf.report()
