library(ggplot2)
library(scales)
library(extrafont)
font_import()
# https://stackoverflow.com/questions/27689222/changing-fonts-for-graphs-in-r

ps=1
ats=25
ts=25

data <- read.csv("cma_logmore.csv", header=TRUE, sep=",")

pdf(file="tp-latency_scatterall.pdf", height=1.75, width=3)
f_data <- subset(data, target=="DRAM_cycle")

#f_data$latency_partition <- factor(f_data$latency_partition, levels=f_data$latency_partition[order(f_data$evo_counter)])

print(nrow(f_data))
print(f_data$sample_no)

write.table(f_data,row.names=F,quote=F,"tput_latency_scatter.csv")

f_data<-f_data[f_data$seeding_type=="allzeros",]

p <- ggplot(f_data,aes(y=as.numeric(tp_gain), x=as.numeric(evo_counter)))
p <- p + geom_point(size=ps, aes(color=as.factor(evo_counter)))
p<-p+labs(x="Evolution Count", y="Throughput Gain")
#p<-p+facet_grid(~seeding_type)
p<-p+theme_bw()
p<-p+theme(text=element_text(family="Times New Roman", size=10))
#p<-p+theme(axis.text=element_text(size=ats))
#p<-p+theme(text=element_text(size=ts))
p<-p+theme(legend.position="None")
# https://felixfan.github.io/ggplot2-remove-grid-background-margin/
p<-p+theme(panel.border = element_blank(), panel.grid.major = element_blank(),
panel.grid.minor = element_blank(), axis.line = element_line(colour = "black",size=0.25))
# https://stackoverflow.com/questions/26367296/how-do-i-make-my-axis-ticks-face-inwards-in-ggplot2
p<-p+theme(axis.ticks.length=unit(-0.15, "cm"),axis.text.x = element_text(color='black',margin=unit(c(0.25,0.25,0.25,0.25), "cm")), axis.text.y = element_text(color='black',margin=unit(c(0.25,0.25,0.25,0.25), "cm")))
# https://stackoverflow.com/questions/36474643/graphical-parameters-in-ggplot2-how-to-change-axis-tick-thickness
p<-p+theme(axis.ticks = element_line(colour = "grey", size = 0.25))

p

